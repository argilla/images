# cgi-python

## Istruzioni per aggiungere una nuova applicazione CGI Python

1. Assicurarsi che siano rispettate le seguenti convenzioni:
  * script CGI nominato come `main.py`;
  * eventuale presenza di un `requirements.txt` con la lista di dipendenze pip;
  * eventuale presenza di una cartella `conf` dove lo script CGI deve caricare la configurazione da un file `conf.custom.ini`;
1. Procurarsi l'URL del repository git su cui è versionato il progetto
1. Inserire l'URL in `install.sh` accondandolo nell'array di repository.
1. Committare, pushare e chiedere al capofficina di aggiornare il container.