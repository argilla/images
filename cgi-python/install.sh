#!/bin/bash

function die {
    echo -e "Qualcosa è andato storto"
    exit -1
}

DEPLOY_DIR="/srv/cgi"
CFG_DIR="/srv/cfg"

mkdir -p $DEPLOY_DIR
mkdir -p $CFG_DIR

# Lista dei repository CGI
REPO[0]="https://git.golem.linux.it/golem/zerocalcare.git"
REPO[1]="https://git.golem.linux.it/golem/tpdf.git"
# ... aggiungi qui altri repository, basta che rispettino le specifiche
# descritte nel readme

cd $DEPLOY_DIR

# Che bello il bash e la sua sintassi creativa, no?
for url in ${REPO[*]}; do
  PRJ_DIR=$(basename "$url" .git)

  git clone $url
  test $? -eq 0 || die

  pushd $PRJ_DIR

  # Linka la configurazione nel sottovolume
  if [ -d conf ]; then
    ln -s $CFG_DIR/$PRJ_DIR.conf.ini conf/conf.custom.ini
    test $? -eq 0 || die
  fi
  
  if [ -f requirements.txt ]; then
    pip install -r requirements.txt
    test $? -eq 0 || die
  fi

  popd
done

exit 0