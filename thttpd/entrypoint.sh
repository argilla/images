#!/bin/sh

# On first startup, generate new ssh key
if [[ $(ls -1 /etc/ssh/ssh_host_*_key | wc -l) -eq 0 ]]; then
    ssh-keygen -A
fi

# Change ssh user password if required
if [[ ! -z "${THTTPD_SSH_USER}" && ! -z "${THTTPD_SSH_PASSWORD}" ]]; then
    echo "${THTTPD_SSH_USER}:${THTTPD_SSH_PASSWORD}" | chpasswd
fi

/usr/sbin/sshd -f /etc/ssh/sshd_config
thttpd -D -d /html

